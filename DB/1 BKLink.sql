USE [master]
GO
/****** Object:  Database [BKLink]    Script Date: 2018-12-28 12:16:49 ******/
CREATE DATABASE [BKLink] ON  PRIMARY 
( NAME = N'BKLink', FILENAME = N'D:\BKLinkServer\DB\BKLink.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'BKLink_log', FILENAME = N'D:\BKLinkServer\DB\BKLink.ldf' , SIZE = 11264KB , MAXSIZE = 2048GB , FILEGROWTH = 10240KB )
GO
ALTER DATABASE [BKLink] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BKLink].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BKLink] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BKLink] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BKLink] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BKLink] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BKLink] SET ARITHABORT OFF 
GO
ALTER DATABASE [BKLink] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BKLink] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BKLink] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BKLink] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BKLink] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BKLink] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BKLink] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BKLink] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BKLink] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BKLink] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BKLink] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BKLink] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BKLink] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BKLink] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BKLink] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BKLink] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BKLink] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BKLink] SET RECOVERY FULL 
GO
ALTER DATABASE [BKLink] SET  MULTI_USER 
GO
ALTER DATABASE [BKLink] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BKLink] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BKLink', N'ON'
GO
USE [BKLink]
GO
/****** Object:  Table [dbo].[T_KdVersion]    Script Date: 2018-12-28 12:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_KdVersion](
	[FID] [bigint] IDENTITY(1,1) NOT NULL,
	[FPRODUCTNAME] [nvarchar](200) NOT NULL,
	[FVERSION] [nvarchar](200) NULL,
	[FVBDLLNAME] [nvarchar](200) NOT NULL,
	[FCODE] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_T_KDVERSION] PRIMARY KEY CLUSTERED 
(
	[FID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_SyncDataInfo]    Script Date: 2018-12-28 12:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_SyncDataInfo](
	[FID] [bigint] IDENTITY(1,1) NOT NULL,
	[FTEMPID] [bigint] NOT NULL,
	[FBILLFORMID] [nvarchar](200) NOT NULL,
	[FLASTESTSYNCDATE] [datetime] NULL,
	[FMAXBILLID] [bigint] NULL,
	[FBILLIDS_FAIL] [nvarchar](max) NULL,
	[FMAXMODIFYTIME] [varchar](50) NULL,
 CONSTRAINT [PK_T_SYNCDATAINFO_PRIMARYKEY] PRIMARY KEY CLUSTERED 
(
	[FID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_T_SYNCDATAINFO_TEMPID_FORMID] UNIQUE NONCLUSTERED 
(
	[FBILLFORMID] ASC,
	[FTEMPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_Template]    Script Date: 2018-12-28 12:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Template](
	[FID] [bigint] IDENTITY(1,1) NOT NULL,
	[FTYPE] [int] NOT NULL,
	[FVERSIONID] [int] NULL,
	[FBILLFORMID] [nvarchar](200) NOT NULL,
	[FBILLNAME] [nvarchar](500) NULL,
	[FPRIMARYKEY] [nvarchar](200) NOT NULL,
	[FBILLFORMID_B] [nvarchar](500) NOT NULL,
	[FBILLNAME_B] [nvarchar](500) NULL,
	[FISSYNC] [bit] NULL,
	[FINTERVAL] [int] NULL,
	[FISINUSE] [bit] NULL,
	[FDATETIME] [datetime] NULL CONSTRAINT [DF_T_Template_FDATETIME]  DEFAULT (getdate()),
	[FTITLE] [nvarchar](500) NULL,
	[FFILTER] [nvarchar](500) NULL,
 CONSTRAINT [PK_T_TEMPLATE] PRIMARY KEY CLUSTERED 
(
	[FID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_TemplateEntry]    Script Date: 2018-12-28 12:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_TemplateEntry](
	[FID] [bigint] NULL,
	[FENTRYID] [bigint] IDENTITY(1,1) NOT NULL,
	[FISCUSTOMFIELD] [bit] NULL,
	[FFIELDNAME_B] [nvarchar](500) NULL,
	[FFIELDKEY_B] [nvarchar](500) NOT NULL,
	[FFIELDDATETYPE_B] [nvarchar](500) NULL,
	[FSRCBILLFORMID_B] [nvarchar](500) NULL,
	[FSRCFIELDNAME_B] [nvarchar](500) NULL,
	[FSRCFIELDKEY_B] [nvarchar](500) NULL,
	[FSRCFIELDDATATYPE_B] [nvarchar](500) NULL,
	[FENTITY] [nvarchar](500) NOT NULL,
	[FTYPE] [int] NOT NULL,
	[FPAGE] [int] NULL CONSTRAINT [DF_T_TemplateEntry_FPAGE]  DEFAULT ((1)),
	[FFIELDNAME] [nvarchar](500) NULL,
	[FFIELDKEY] [nvarchar](500) NOT NULL,
	[FISMUST] [bit] NOT NULL,
	[FFIELDDATETYPE] [nvarchar](500) NULL,
	[FSRCBILLFORMID] [nvarchar](500) NULL,
	[FSRCBILLENTITY] [nvarchar](500) NULL,
	[FSRCPRIMARYKEY] [nvarchar](30) NULL,
	[FSRCFIELDNAME] [nvarchar](500) NULL,
	[FSRCFIELDKEY] [nvarchar](500) NULL,
	[FSRCFIELDDATATYPE] [nvarchar](500) NULL,
	[FDEFALUTVALUE] [nvarchar](500) NULL,
	[FC] [nvarchar](500) NULL CONSTRAINT [DF_T_TemplateEntry_FC]  DEFAULT (N'?'),
 CONSTRAINT [PK_T_TemplateEntry] PRIMARY KEY CLUSTERED 
(
	[FENTRYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[T_UserInfo]    Script Date: 2018-12-28 12:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_UserInfo](
	[FID] [bigint] IDENTITY(1,1) NOT NULL,
	[FUSERNAME] [nvarchar](200) NOT NULL,
	[FPASSWORD] [nvarchar](500) NOT NULL,
	[FINFO] [nvarchar](max) NOT NULL,
	[FIP] [varchar](50) NULL,
	[FDATE] [datetime] NULL,
	[FPHONENO] [varchar](50) NOT NULL,
	[FEMAIL] [nvarchar](200) NULL,
 CONSTRAINT [PK_T_USERINFO] PRIMARY KEY CLUSTERED 
(
	[FID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'产品名称（商贸版、专业版等）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_KdVersion', @level2type=N'COLUMN',@level2name=N'FPRODUCTNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'版本号（5.0，6.0，6.1等）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_KdVersion', @level2type=N'COLUMN',@level2name=N'FVERSION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'vb组件名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_KdVersion', @level2type=N'COLUMN',@level2name=N'FVBDLLNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'记录ID（自增）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FTEMPID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FBILLFORMID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近同步日期（精确到毫秒）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FLASTESTSYNCDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已同步最大单据ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FMAXBILLID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'同步失败的单据ID（1001,1002,1003）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FBILLIDS_FAIL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已同步的单据的最大修改日期的timestamp' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_SyncDataInfo', @level2type=N'COLUMN',@level2name=N'FMAXMODIFYTIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模板ID（自增）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接口数据传递方向（金蝶到帮我吧 1，帮我吧到金蝶 2）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FTYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金蝶版本id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FVERSIONID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金蝶单据标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FBILLFORMID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金蝶单据名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FBILLNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金蝶单据主键字段名（finterid,fid)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FPRIMARYKEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帮我吧工单模板标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FBILLFORMID_B'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'帮我吧工单模板名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FBILLNAME_B'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否同步' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FISSYNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'同步时间间隔（分）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FINTERVAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FISINUSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近修改日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_Template', @level2type=N'COLUMN',@level2name=N'FDATETIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否帮我吧自定义字段，主要用于金蝶数据同步到帮我吧中，如果是自定义字段添加到custom_fields中，否则添加到顶级。null或1为true，0为false' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_TemplateEntry', @level2type=N'COLUMN',@level2name=N'FISCUSTOMFIELD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'关联基础资料主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_TemplateEntry', @level2type=N'COLUMN',@level2name=N'FSRCPRIMARYKEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FUSERNAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FPASSWORD'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'硬件信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FINFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FIP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最近认证时间（精确到秒）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FDATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FPHONENO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'T_UserInfo', @level2type=N'COLUMN',@level2name=N'FEMAIL'
GO
USE [master]
GO
ALTER DATABASE [BKLink] SET  READ_WRITE 
GO
