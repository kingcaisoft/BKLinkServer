CREATE TABLE #temptable ( [FID] bigint, [FPRODUCTNAME] nvarchar(200), [FVERSION] nvarchar(200), [FVBDLLNAME] nvarchar(200), [FCODE] nvarchar(200) )
INSERT INTO #temptable
VALUES
( 1, N'KIS专业版', N'12.3', N'xxx', N'kiszyb' ), 
( 2, N'KIS专业版', N'15.0', N'xxx', N'kiszyb' ), 
( 3, N'KIS专业版', N'15.1', N'xxx', N'kiszyb' ), 
( 4, N'KIS商贸版', N'7.0', N'xxx', N'kissmb' ), 
( 5, N'KIS旗舰版', N'5.0', N'xxx', N'kisqjb' ), 
( 6, N'K3/Wise', N'15.0', N'xxx', N'k3wise' ), 
( 7, N'K3/Cloud', N'7.1', N'yyy', N'k3cloud' )

insert into T_KdVersion  ( FPRODUCTNAME,FVERSION,FVBDLLNAME,FCODE )
select FPRODUCTNAME,FVERSION,FVBDLLNAME,FCODE from  #temptable

DROP TABLE #temptable
